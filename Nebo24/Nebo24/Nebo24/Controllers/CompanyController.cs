﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nebo24.DAL;
using Nebo24.Models;
using Nebo24.Helpers;
using Nebo24.ViewModels;
using System.Data.Entity.Infrastructure;

namespace Nebo24.Controllers
{
    public class CompanyController : BaseController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Company
        public async Task<ActionResult> Index()
        {
            var model = db.Companies.Include(al => al.HeadquartersAdmLoc).Include(gl => gl.HeadquartersGeoLoc).OrderByDescending(fc => fc.Fleet.Count).ThenBy(a => a.Name);
            return View(await model.ToListAsync());
        }


        // GET: Company/Details/5       
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companies.Include(g => g.HeadquartersAdmLoc).Include(g => g.HeadquartersGeoLoc).SingleOrDefaultAsync(c => c.CompanyID == id);
            if (company == null)
            {
                return HttpNotFound();
            }
            CompanyDetailsViewModel viewModel = new CompanyDetailsViewModel();
            viewModel.Company = company;
            viewModel.Fleet = company.Fleet;
            return View(viewModel);
        }
        

        // GET: Company/Create
        [Authorize]
        public ActionResult Create()
        {
            var model = new Company();
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag);
            return View("Editor", model);
        }

        // POST: Company/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Company company)
        {
            if (ModelState.IsValid)
            {
                db.Companies.Add(company);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ErrorMessage = ModelState.Values.SelectMany(v => v.Errors);
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag, selectedAdmLoc: company.HeadquartersAdmLocID);
            return View("Editor", company);
        }

        // GET: Company/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companies.Include(g => g.HeadquartersAdmLoc).Include(g => g.HeadquartersGeoLoc).SingleOrDefaultAsync(c => c.CompanyID == id);
            if (company == null)
            {
                return HttpNotFound();
            }
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag, selectedAdmLoc: company.HeadquartersAdmLocID);
            ViewBag.Editing = true;
            return View("Editor", company);
        }

        // POST: Company/Edit/5
        [Authorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var company = db.Companies.Find(id);
            if (TryUpdateModel(company))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    ViewBag.Editing = true;
                    return View("Editor", company);
                }
            }
            
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag);
            ViewBag.Editing = true;
            return View("Editor", company);
        }

       
        // GET: Company/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            try
            {
                var c = db.Companies.Find(id);
                db.Companies.Remove(c);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
