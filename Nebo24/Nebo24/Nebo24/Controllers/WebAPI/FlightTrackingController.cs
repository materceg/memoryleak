﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Nebo24.DAL;
using Nebo24.Models;
using Nebo24.Helpers;

namespace Nebo24.Controllers
{
    public class FlightTrackingController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();
        // GET api/<controller>

        public List<CommercialFlight> GetFlightsInProgress()
        {
            var Now = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Central European Standard Time");
            var flights = db.CommercialFlights.Where(flight => ( (!flight.ATA.HasValue || ( flight.ATA > Now))) && flight.ATD.HasValue ).Where(flight => flight.ATD < Now);           
            CommercialFlightCalculator.getStatus(flights);
            return flights.ToList();
        }
    }
}