﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Nebo24.DAL;
using Nebo24.Models;

namespace Nebo24.Controllers.WebAPI
{
    public class AirportController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET api/Airports
        public List<Airport> GetAll()
        {
            var airports = db.Airports;
            return airports.ToList();
        }

        public List<Airport> GetPairByAirportIds(int? idA, int? idB)
        {
            List<Airport> Airports = new List<Airport>();
            Airports.Add(db.Airports.Find(idA));
            Airports.Add(db.Airports.Find(idB));
            return Airports;
        }

        public Airport GetByAirportId(int? id)
        {
            return db.Airports.Find(id);
        }
    }
}
