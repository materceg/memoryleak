﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Nebo24.Models;
using Nebo24.DAL;

namespace Nebo24.Controllers.WebAPI
{
    public class AirplaneTypeController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();
        public AirplaneType GetByAirplaneTypeId(int? id)
        {
            return db.AirplaneTypes.Find(id);
        }
        public AirplaneType GetByAirplaneId(int? id)
        {
            int? airplaneTypeID = db.Airplanes.Find(id).AirplaneTypeID;
            return GetByAirplaneTypeId(airplaneTypeID);
        }
    }
}
