﻿using Nebo24.DAL;
using Nebo24.Models;
using Nebo24.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Nebo24.Helpers;

namespace Nebo24.Controllers
{
    public class AirplaneTypeController : BaseController
    {

        private DatabaseContext db = new DatabaseContext();

        // GET: AirplaneType
        public async Task<ActionResult> Index()
        {
            var model = db.AirplaneTypes.OrderBy(a => a.Name);
            return View(await model.ToListAsync());
        }

        // GET: AirplaneType/Details/:id
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = new AirplaneTypeViewModel();
            model.Flights = new CommercialFlightsViewModel();

            model.AirplaneType = db.AirplaneTypes.Find(id);  
            model.Airplanes = db.Airplanes.Where(x => x.AirplaneTypeID == id);          
            model.Flights.CommercialFlights = db.CommercialFlights.Where(x => x.Airplane.AirplaneTypeID == id);
            model.Flights.Delay = CommercialFlightCalculator.getDelays(model.Flights.CommercialFlights);
            CommercialFlightCalculator.getStatus(model.Flights.CommercialFlights);
            return View(model);
        }

        // GET: AirplaneType/Create
        [Authorize]
        public ActionResult Create()
        {
            var model = new AirplaneType();
            return View("Editor", model);
        }

        // POST: AirplaneType/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(AirplaneType airplaneType)
        {
            if (ModelState.IsValid)
            {
                db.AirplaneTypes.Add(airplaneType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ErrorMessage = ModelState.Values.SelectMany(v => v.Errors);
            return View("Editor", airplaneType);
        }

        // GET: AirplaneType/Edit/:id
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AirplaneType airplaneType = db.AirplaneTypes.Find(id);
            if (airplaneType == null)
            {
                return HttpNotFound();
            }
            ViewBag.Editing = true;
            return View("Editor", airplaneType);
        }

        // POST: Airplane/Edit/:id
        [Authorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var airplaneType = db.AirplaneTypes.Find(id);
            if (TryUpdateModel(airplaneType))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    ViewBag.Editing = true;
                    View("Editor", airplaneType);
                }
            }
            
            ViewBag.Editing = true;
            return View("Editor", airplaneType);
        }

        // GET: AirplaneType/Delete/:id
        [Authorize]
        public ActionResult Delete(int id)
        {
            try
            {
                var airplaneType = db.AirplaneTypes.Find(id);
                db.AirplaneTypes.Remove(airplaneType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
    }
}