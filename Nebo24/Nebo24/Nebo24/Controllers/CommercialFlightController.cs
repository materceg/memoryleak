﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Nebo24.DAL;
using Nebo24.Models;
using Nebo24.Helpers;
using System.Collections.Generic;
using Nebo24.ViewModels;

namespace Nebo24.Controllers
{
    public class CommercialFlightController : BaseController
    {
        private DatabaseContext db = new DatabaseContext();

      
        // GET: CommercialFlight
        public ActionResult Index(DateTime? DepartureTime, DateTime? ArrivalTime, int? DepartureAirportID, int? DestinationAirportID)
        {
            AirportHelper.PopulateAirportsDropDownList(db, ViewBag);

            CommercialFlightsViewModel model = new CommercialFlightsViewModel();
            IQueryable<CommercialFlight> flightList;

            if (DepartureTime.HasValue && ArrivalTime.HasValue) //get flights that should depart in the specified time span
                flightList = db.CommercialFlights.Where(flight => (flight.STD >= DepartureTime) && (flight.STD <= ArrivalTime)).Include(flight => flight.Airplane).Include(flight => flight.DepartureAirport).Include(flight => flight.DestinationAirport);
            else if (DepartureTime.HasValue) //get flights that should depart after the specified date 
                flightList = db.CommercialFlights.Where(flight => (flight.STD >= DepartureTime)).Include(flight => flight.Airplane).Include(flight => flight.DepartureAirport).Include(flight => flight.DestinationAirport);
            else if (ArrivalTime.HasValue)  //get flight that should depart before the specified date
                flightList = db.CommercialFlights.Where(flight => (flight.STD <= ArrivalTime)).Include(flight => flight.Airplane).Include(flight => flight.DepartureAirport).Include(flight => flight.DestinationAirport);
            else //get all flights
                flightList = db.CommercialFlights.Include(flight => flight.Airplane).Include(flight => flight.DepartureAirport).Include(flight => flight.DestinationAirport);

            if (!User.Identity.IsAuthenticated)                                                                    
            {
                DateTime pastBorder = DateTime.Now.AddDays(-7);
                DateTime futureBorder = DateTime.Now.AddDays(7);
                flightList = flightList.Where(flight => (flight.STD >= pastBorder && flight.STA <= futureBorder));                      
            }

            if (DepartureAirportID.HasValue && DestinationAirportID.HasValue)
                flightList = flightList.Where(flight => flight.DepartureAirportID == DepartureAirportID && flight.DestinationAirportID == DestinationAirportID);
            else if (DepartureAirportID.HasValue)
                flightList = flightList.Where(flight => flight.DepartureAirportID == DepartureAirportID);
            else if (DestinationAirportID.HasValue)
                flightList = flightList.Where(flight => flight.DestinationAirportID == DestinationAirportID);

            model.CommercialFlights = flightList.OrderByDescending(t => t.STD).ThenByDescending(t => t.STD);
            model.Delay = CommercialFlightCalculator.getDelays(flightList);

            //get status
            CommercialFlightCalculator.getStatus(flightList);

            model.Filter = new FlightsFilterViewModel();
            
            return View(model);
        }


        // GET: CommercialFlight/Details/5       
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommercialFlight comFlight = db.CommercialFlights.Find(id);
            if (comFlight == null)
            {
                return HttpNotFound();
            }
            CommercialFlightViewModel model = new CommercialFlightViewModel();
            model.CommercialFlight = comFlight;
            model.DepartureAirport = model.CommercialFlight.DepartureAirport;
            model.DestinationAirport = model.CommercialFlight.DestinationAirport;

            if (model.CommercialFlight.ATA.HasValue)
            {
                if (model.CommercialFlight.ATA - model.CommercialFlight.STA >= TimeSpan.FromMinutes(30))
                    model.Delay = (model.CommercialFlight.ATA - model.CommercialFlight.STA).Value.TotalMinutes;
            }
            else if (model.CommercialFlight.ATD.HasValue)
            {
                if (model.CommercialFlight.ATD - model.CommercialFlight.STD >= TimeSpan.FromMinutes(15))
                    model.Delay = (model.CommercialFlight.ATD - model.CommercialFlight.STD).Value.TotalMinutes;
            }
            else
                model.Delay = null;


            CommercialFlightCalculator.getStatus(model.CommercialFlight);

            //CommercialFlight comFlight = db.CommercialFlights.Where(flight => flight.AirplaneID == id).Include(flight => flight.DepartureAirport).Include(flight => flight.DestinationAirport).First();

            return View(model);
        }
        


        //----------------------------------------------------------------------------------
        // GET: CommercialFlight/Create
        [Authorize]
        public ActionResult Create()
        {
            var model = new CommercialFlight();
            
            AirplaneHelper.PopulateAirplaneDropDownList(db, ViewBag);
            AirportHelper.PopulateAirportsDropDownList(db, ViewBag);

            return View("Editor", model);           
        }

        // POST: CommercialFlight/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(CommercialFlight comFlight)
        {
            if (ModelState.IsValid)
            {
                db.CommercialFlights.Add(comFlight);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ErrorMessage = ModelState.Values.SelectMany(v => v.Errors);

            AirportHelper.PopulateAirportsDropDownList(db, ViewBag, comFlight.DepartureAirportID);
            AirplaneHelper.PopulateAirplaneDropDownList(db, ViewBag, selectedAirplane: comFlight.AirplaneID);

            return View("Editor", comFlight);
        }
        //--------------------------------------------------------------------------------
        


        //-------------------------------------------------------------------------------       
        // GET: CommercialFlight/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommercialFlight comFlight = db.CommercialFlights.Find(id);
            if (comFlight == null)
            {
                return HttpNotFound();
            }
            
            AirplaneHelper.PopulateAirplaneDropDownList(db, ViewBag, selectedAirplane: comFlight.AirplaneID);
            AirportHelper.PopulateAirportsDropDownList(db, ViewBag, 
                selectedDepartureAirport: comFlight.DepartureAirportID, selectedDestinationAirport: comFlight.DestinationAirportID);

            ViewBag.Editing = true;
            return View("Editor", comFlight);                               
        }


      

        // POST: CommercialFlight/Edit/5
        [Authorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var comFlight = db.CommercialFlights.Find(id);
            if (TryUpdateModel(comFlight))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    ViewBag.Editing = true;
                    View("Editor", comFlight);
                }
            }

            ViewBag.DepartureAirports = new SelectList(db.Airports, "AirportID", "Departure Airport");
            ViewBag.DestinationAirports = new SelectList(db.Airports, "AirportID", "Destination Airport");
            AirplaneHelper.PopulateAirplaneDropDownList(db, ViewBag);
            ViewBag.Editing = true;
            return View("Editor", comFlight);
        }
        //----------------------------------------------------------------------------



        //-------------------------------------------------------------------------------
        // GET: CommercialFlight/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            try
            {
                var comFlight = db.CommercialFlights.Find(id);
                db.CommercialFlights.Remove(comFlight);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
        //-------------------------------------------------------------------------------            

    }
}
