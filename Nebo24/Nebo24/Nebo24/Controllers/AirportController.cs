﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nebo24.DAL;
using System.Data.Entity;
using System.Threading.Tasks;
using Nebo24.Models;
using Nebo24.Helpers;
using System.Net;
using Nebo24.ViewModels;
using System.Data.Entity.Core.Objects;
using System.Linq.Expressions;

namespace Nebo24.Controllers
{
    public class AirportController : BaseController
    {

        private DatabaseContext db = new DatabaseContext();

        // GET: Airport
        public async Task<ActionResult> Index()
        {
            var model = db.Airports.Include(al => al.AdmLoc).OrderBy(ap => ap.Name);
            return View(await model.ToListAsync());
        }
        
        // GET: Airport/Details/5
        public ActionResult Details(int? id, DateTime? DepartureTime, DateTime? ArrivalTime)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AirportViewModel model = new AirportViewModel();
            model.Filter = new FlightsFilterViewModel();
            model.Departures = new CommercialFlightsViewModel();
            model.Arrivals = new CommercialFlightsViewModel();
            model.Airport = db.Airports.Find(id);
            model.Departures.CommercialFlights = db.CommercialFlights.Where(d => d.DepartureAirportID == id);
            model.Arrivals.CommercialFlights = db.CommercialFlights.Where(a => a.DestinationAirportID == id);


            //filter departures and arrivals
            if (DepartureTime.HasValue && ArrivalTime.HasValue)  //get flights that should depart from this airport, or arrive in this airport in the specified time span 
            {
                model.Filter.DepartureTime = DepartureTime;
                model.Filter.ArrivalTime = ArrivalTime;
                model.Departures.CommercialFlights = model.Departures.CommercialFlights.Where(flight => (flight.STD >= DepartureTime) && (flight.STD <= ArrivalTime)); //i deleted all includes !!!!!!!!
                model.Arrivals.CommercialFlights = model.Arrivals.CommercialFlights.Where(flight => (flight.STA >= DepartureTime) && (flight.STA <= ArrivalTime)); 
            }
            else if (DepartureTime.HasValue) //get flights that should depart from this airport, or arrive in this airport after the specified date 
            {
                model.Filter.DepartureTime = DepartureTime;
                model.Departures.CommercialFlights = model.Departures.CommercialFlights.Where(flight => (flight.STD >= DepartureTime)); //i deleted all includes !!!!!!!!
                model.Arrivals.CommercialFlights = model.Arrivals.CommercialFlights.Where(flight => (flight.STA >= DepartureTime));
            }
            else if (ArrivalTime.HasValue) //get flights that should depart from this airport, or arrive in this airport before the specified date
            {
                model.Filter.ArrivalTime = ArrivalTime;
                model.Departures.CommercialFlights = model.Departures.CommercialFlights.Where(flight =>  (flight.STD <= ArrivalTime)); //i deleted all includes !!!!!!!!
                model.Arrivals.CommercialFlights = model.Arrivals.CommercialFlights.Where(flight =>  (flight.STA <= ArrivalTime));
            }
            
            //extra filter if user isn't authenticated
            if (!User.Identity.IsAuthenticated)
            {
                DateTime pastBorder = DateTime.Now.AddDays(-7);
                DateTime futureBorder = DateTime.Now.AddDays(7);
                model.Departures.CommercialFlights = model.Departures.CommercialFlights.Where(flight => (flight.STD >= pastBorder && flight.STA <= futureBorder));
                model.Arrivals.CommercialFlights = model.Arrivals.CommercialFlights.Where(flight => (flight.STD >= pastBorder && flight.STA <= futureBorder));
            }


            // get average delays for this airport (filtered flight lists)
            try {
                model.AverageDepartureDelay = model.Departures.CommercialFlights.Where(flight => flight.ATD.HasValue).Average(flight => EntityFunctions.DiffMinutes(flight.STD, flight.ATD).Value);
            }
            catch (InvalidOperationException ex)
            {
                model.AverageDepartureDelay = 0;
            }

            try {
                model.AverageArrivalDelay = model.Arrivals.CommercialFlights.Where(flight => flight.ATA.HasValue).Average(flight => EntityFunctions.DiffMinutes(flight.STA, flight.ATA).Value);
            }
            catch (InvalidOperationException ex) {
                model.AverageArrivalDelay = 0;
            }

            // get delays for each flight
            model.Departures.Delay = CommercialFlightCalculator.getDelays(model.Departures.CommercialFlights);
            model.Arrivals.Delay = CommercialFlightCalculator.getDelays(model.Arrivals.CommercialFlights);

            //get status for flights
            CommercialFlightCalculator.getStatus(model.Departures.CommercialFlights);
            CommercialFlightCalculator.getStatus(model.Arrivals.CommercialFlights);

            return View(model);
        }
        

        // GET: Airport/Create
        [Authorize]
        public ActionResult Create()
        {
            var model = new Airport();
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag);
            return View("Editor", model);
        }

        // POST: Airport/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Airport airport)
        {
            if (ModelState.IsValid)
            {
                db.Airports.Add(airport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ErrorMessage = ModelState.Values.SelectMany(v => v.Errors);
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag, selectedAdmLoc: airport.AdministrativeLocationID);
            return View("Editor", airport);          
        }

        // GET: Airport/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Airport airport = db.Airports.Find(id);
            if (airport == null)
            {
                return HttpNotFound();
            }
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag, selectedAdmLoc: airport.AdministrativeLocationID);
            ViewBag.Editing = true;
            return View("Editor", airport);
        }

        // POST: Airport/Edit/5
        [Authorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var airport = db.Airports.Find(id);
            if (TryUpdateModel(airport))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    ViewBag.Editing = true;                    
                    return View("Editor", airport);
                }
            }
       
            AdministrativeLocationHelper.PopulateAdministrativelocationsDropDownList(db, ViewBag);
            ViewBag.Editing = true;
            return View("Editor", airport);
        }

        
        // GET: Airport/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            try
            {
                var airport = db.Airports.Find(id);
                db.Airports.Remove(airport);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        #region  Old details
        /*
        // GET: Airport/Details/5
        public ActionResult Details(int? id, DateTime? item1, DateTime? item2)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AirportViewModel model = new AirportViewModel();
            Dictionary<int, double> averageDepartureDelays = new Dictionary<int, double>();
            Dictionary<int, double> averageArrivalDelays = new Dictionary<int, double>();
            var airports = db.Airports.Include(al => al.AdmLoc).Include(flights => flights.ArrivingFlights).Include(flights => flights.DepartingFlights);

            foreach (Airport airport in airports)
            {
                model.Departures = new CommercialFlightsViewModel();
                model.Departures.CommercialFlights = db.CommercialFlights.Where(d => d.DepartureAirportID == id);
                try
                {
                    averageArrivalDelays.Add(airport.AirportID, airport.ArrivingFlights.Where(flight => flight.ATA.HasValue).Average(flight => (flight.ATA - flight.STA).Value.TotalMinutes));
                }
                catch (InvalidOperationException ex)  //if there are no arriving flights 
                {
                    averageArrivalDelays.Add(airport.AirportID, 0);
                }
                try
                {
                    model.Arrivals = new CommercialFlightsViewModel();
                    model.Arrivals.CommercialFlights = db.CommercialFlights.Where(a => a.DestinationAirportID == id);
                    averageDepartureDelays.Add(airport.AirportID, airport.DepartingFlights.Where(flight => flight.ATD.HasValue).Average(flight => (flight.ATD - flight.STD).Value.TotalMinutes));
                }
                catch (InvalidOperationException ex)  //if there are no departing flights
                {
                    averageDepartureDelays.Add(airport.AirportID, 0);
                }
            }

            model.Airport = db.Airports.Find(id);
            model.AverageArrivalDelays = averageArrivalDelays;
            model.AverageDepartureDelays = averageDepartureDelays;
            return View(model);
        }
        */
        #endregion

    }
}
