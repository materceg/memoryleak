﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Nebo24.DAL;
using Nebo24.Models;
using Nebo24.Helpers;
using System.Net;

namespace Nebo24.Controllers
{
    public class AirplaneController : BaseController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Airplane
        public async Task<ActionResult> Index()
        {
            var model = db.Airplanes.Include(ap => ap.Company).OrderBy(ap => ap.Type.Name).ThenBy(ap => ap.Company.Name).ThenBy(ap => ap.Registration);
            return View(await model.ToListAsync());
        }

        // GET: Airplane/Create
        [Authorize]
        public ActionResult Create()
        {
            var model = new Airplane();
            PopulateAirplaneTypeDropDownList();
            PopulateCompaniesDropDownList();
            return View("Editor", model);
        }

        // POST: Airplane/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Airplane airplane)
        {
            if (ModelState.IsValid)
            {
                db.Airplanes.Add(airplane);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ErrorMessage = ModelState.Values.SelectMany(v => v.Errors);
            return View("Editor", airplane);
        }

        // GET: Airplane/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }           
            Airplane airplane = db.Airplanes.Find(id);
            if (airplane == null)
            {
                return HttpNotFound();
            }
            PopulateAirplaneTypeDropDownList(airplane.AirplaneTypeID);
            PopulateCompaniesDropDownList(airplane.CompanyID);
            ViewBag.Editing = true;
            return View("Editor", airplane);
        }

        // POST: Airplane/Edit/5
        [Authorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var airplane = db.Airplanes.Find(id);
            if (TryUpdateModel(airplane))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = e.Message;
                    ViewBag.Editing = true;
                    View("Editor", airplane);
                }
            }
           
            PopulateAirplaneTypeDropDownList();
            PopulateCompaniesDropDownList();
            ViewBag.Editing = true;
            return View("Editor", airplane);
        }

        // GET: Airplane/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            try
            {
                var airplane = db.Airplanes.Find(id);
                db.Airplanes.Remove(airplane);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       
        private void PopulateAirplaneTypeDropDownList(object selectedAirplaneType = null)
        {
            ViewBag.AirplaneTypes = new SelectList(db.AirplaneTypes, "AirplaneTypeID", "Name", selectedAirplaneType);
        }
        private void PopulateCompaniesDropDownList(object selectedCompany = null)
        {
            ViewBag.Companies = new SelectList(db.Companies, "CompanyID", "Name", selectedCompany);
        }
    }
}
