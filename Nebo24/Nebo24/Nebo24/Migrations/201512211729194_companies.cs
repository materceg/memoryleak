namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class companies : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AdmLoc_AdministrativeLocationID = c.Int(nullable: false),
                        GeoLoc_GeographicalLocationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.AdministrativeLocations", t => t.AdmLoc_AdministrativeLocationID, cascadeDelete: true)
                .ForeignKey("dbo.GeographicalLocations", t => t.GeoLoc_GeographicalLocationID, cascadeDelete: true)
                .Index(t => t.AdmLoc_AdministrativeLocationID)
                .Index(t => t.GeoLoc_GeographicalLocationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "GeoLoc_GeographicalLocationID", "dbo.GeographicalLocations");
            DropForeignKey("dbo.Companies", "AdmLoc_AdministrativeLocationID", "dbo.AdministrativeLocations");
            DropIndex("dbo.Companies", new[] { "GeoLoc_GeographicalLocationID" });
            DropIndex("dbo.Companies", new[] { "AdmLoc_AdministrativeLocationID" });
            DropTable("dbo.Companies");
        }
    }
}
