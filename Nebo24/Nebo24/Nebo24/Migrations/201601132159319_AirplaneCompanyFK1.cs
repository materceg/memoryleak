namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AirplaneCompanyFK1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Airplanes", "AirplaneTypeID", "dbo.AirplaneTypes");
            DropIndex("dbo.Airplanes", new[] { "AirplaneTypeID" });
            AlterColumn("dbo.Airplanes", "AirplaneTypeID", c => c.Int());
            CreateIndex("dbo.Airplanes", "AirplaneTypeID");
            AddForeignKey("dbo.Airplanes", "AirplaneTypeID", "dbo.AirplaneTypes", "AirplaneTypeID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Airplanes", "AirplaneTypeID", "dbo.AirplaneTypes");
            DropIndex("dbo.Airplanes", new[] { "AirplaneTypeID" });
            AlterColumn("dbo.Airplanes", "AirplaneTypeID", c => c.Int(nullable: false));
            CreateIndex("dbo.Airplanes", "AirplaneTypeID");
            AddForeignKey("dbo.Airplanes", "AirplaneTypeID", "dbo.AirplaneTypes", "AirplaneTypeID", cascadeDelete: true);
        }
    }
}
