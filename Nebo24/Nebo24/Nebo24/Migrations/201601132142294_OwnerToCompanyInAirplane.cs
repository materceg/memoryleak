namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OwnerToCompanyInAirplane : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Airplanes", name: "Owner_CompanyID", newName: "CompanyID");
            RenameIndex(table: "dbo.Airplanes", name: "IX_Owner_CompanyID", newName: "IX_CompanyID");
            DropColumn("dbo.Airplanes", "OwnerID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Airplanes", "OwnerID", c => c.Int());
            RenameIndex(table: "dbo.Airplanes", name: "IX_CompanyID", newName: "IX_Owner_CompanyID");
            RenameColumn(table: "dbo.Airplanes", name: "CompanyID", newName: "Owner_CompanyID");
        }
    }
}
