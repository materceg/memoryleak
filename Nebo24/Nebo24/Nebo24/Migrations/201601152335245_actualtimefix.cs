namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class actualtimefix : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CommercialFlights", "ATD", c => c.DateTime());
            AlterColumn("dbo.CommercialFlights", "ATA", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CommercialFlights", "ATA", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CommercialFlights", "ATD", c => c.DateTime(nullable: false));
        }
    }
}
