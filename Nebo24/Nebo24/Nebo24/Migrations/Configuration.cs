namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Nebo24.DAL.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Nebo24.DAL.DatabaseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            /*
            context.AdministrativeLocations.AddOrUpdate(
                  al => al.City,
                  new Models.AdministrativeLocation { City = "Zagreb", Country = "Hrvatska" },
                  new Models.AdministrativeLocation { City = "Osijek", Country = "Hrvatska" },
                  new Models.AdministrativeLocation { City = "Split", Country = "Hrvatska" },
                  new Models.AdministrativeLocation { City = "Rijeka", Country = "Hrvatska" },
                  new Models.AdministrativeLocation { City = "Ljubljana", Country = "Slovenija" },
                  new Models.AdministrativeLocation { City = "London", Country = "United Kingdom" },
                  new Models.AdministrativeLocation { City = "Frankfurt am Main", Country = "Deutschland" },
                  new Models.AdministrativeLocation { City = "Moskva", Country = "Rossiyskaya Federatsiya" },
                  new Models.AdministrativeLocation { City = "New York", Country = "United States" },
                  new Models.AdministrativeLocation { City = "Los Angeles", Country = "United States" },
                  new Models.AdministrativeLocation { City = "Boston", Country = "United States" },
                  new Models.AdministrativeLocation { City = "Houston", Country = "United States" },
                  new Models.AdministrativeLocation { City = "New York", Country = "United States" },
                  new Models.AdministrativeLocation { City = "New York", Country = "United States" },
                  new Models.AdministrativeLocation { City = "Tokio", Country = "Nippon" },
                  new Models.AdministrativeLocation { City = "Seoul", Country = "Korea Republic" },
                  new Models.AdministrativeLocation { City = "Brasilia", Country = "Brasil" },
                  new Models.AdministrativeLocation { City = "Beijing", Country = "China" }

                );
                
            context.AirplaneTypes.AddOrUpdate(
                apt => apt.ICAO,
                new Models.AirplaneType { ICAO = "TK1824", Name = "Turski bomber" }
                );
                */
        }
    }
}
