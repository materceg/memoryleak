namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixAirplane : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Airplanes", "OwnerID", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Airplanes", "OwnerID", c => c.Int(nullable: false));
        }
    }
}
