namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class admloc : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdministrativeLocations",
                c => new
                    {
                        AdministrativeLocationID = c.Int(nullable: false, identity: true),
                        City = c.String(nullable: false, maxLength: 96),
                        Country = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.AdministrativeLocationID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdministrativeLocations");
        }
    }
}
