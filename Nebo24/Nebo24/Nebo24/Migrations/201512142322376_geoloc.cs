namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class geoloc : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GeographicalLocations",
                c => new
                    {
                        GeographicalLocationID = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.GeographicalLocationID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GeographicalLocations");
        }
    }
}
