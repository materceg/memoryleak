namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlightAirplaneFix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Airports", "IATA", c => c.String(nullable: false));
            AddColumn("dbo.CommercialFlights", "DepartureAirportID", c => c.Int());
            AddColumn("dbo.CommercialFlights", "DestinationAirportID", c => c.Int());
            AddColumn("dbo.PrivateFlights", "DepartureAirportID", c => c.Int());
            AddColumn("dbo.PrivateFlights", "DestinationAirportID", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrivateFlights", "DestinationAirportID");
            DropColumn("dbo.PrivateFlights", "DepartureAirportID");
            DropColumn("dbo.CommercialFlights", "DestinationAirportID");
            DropColumn("dbo.CommercialFlights", "DepartureAirportID");
            DropColumn("dbo.Airports", "IATA");
        }
    }
}
