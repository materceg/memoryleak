namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class airplaneOwner : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Airports",
                c => new
                    {
                        AirportID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AdministrativeLocationID = c.Int(),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.AirportID)
                .ForeignKey("dbo.AdministrativeLocations", t => t.AdministrativeLocationID)
                .Index(t => t.AdministrativeLocationID);
            
            CreateTable(
                "dbo.CommercialFlights",
                c => new
                    {
                        FlightID = c.Int(nullable: false, identity: true),
                        number = c.Int(nullable: false),
                        CompanyID = c.Int(),
                        STD = c.DateTime(nullable: false),
                        ATD = c.DateTime(nullable: false),
                        STA = c.DateTime(nullable: false),
                        ATA = c.DateTime(nullable: false),
                        callsign = c.String(nullable: false),
                        status = c.Int(nullable: false),
                        AirplaneID = c.Int(),
                        DepartureAirport_AirportID = c.Int(),
                        DestinationAirport_AirportID = c.Int(),
                    })
                .PrimaryKey(t => t.FlightID)
                .ForeignKey("dbo.Airplanes", t => t.AirplaneID)
                .ForeignKey("dbo.Companies", t => t.CompanyID)
                .ForeignKey("dbo.Airports", t => t.DepartureAirport_AirportID)
                .ForeignKey("dbo.Airports", t => t.DestinationAirport_AirportID)
                .Index(t => t.CompanyID)
                .Index(t => t.AirplaneID)
                .Index(t => t.DepartureAirport_AirportID)
                .Index(t => t.DestinationAirport_AirportID);
            
            CreateTable(
                "dbo.PrivateFlights",
                c => new
                    {
                        FlightID = c.Int(nullable: false, identity: true),
                        callsign = c.String(nullable: false),
                        status = c.Int(nullable: false),
                        AirplaneID = c.Int(),
                        DepartureAirport_AirportID = c.Int(),
                        DestinationAirport_AirportID = c.Int(),
                    })
                .PrimaryKey(t => t.FlightID)
                .ForeignKey("dbo.Airplanes", t => t.AirplaneID)
                .ForeignKey("dbo.Airports", t => t.DepartureAirport_AirportID)
                .ForeignKey("dbo.Airports", t => t.DestinationAirport_AirportID)
                .Index(t => t.AirplaneID)
                .Index(t => t.DepartureAirport_AirportID)
                .Index(t => t.DestinationAirport_AirportID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrivateFlights", "DestinationAirport_AirportID", "dbo.Airports");
            DropForeignKey("dbo.PrivateFlights", "DepartureAirport_AirportID", "dbo.Airports");
            DropForeignKey("dbo.PrivateFlights", "AirplaneID", "dbo.Airplanes");
            DropForeignKey("dbo.CommercialFlights", "DestinationAirport_AirportID", "dbo.Airports");
            DropForeignKey("dbo.CommercialFlights", "DepartureAirport_AirportID", "dbo.Airports");
            DropForeignKey("dbo.CommercialFlights", "CompanyID", "dbo.Companies");
            DropForeignKey("dbo.CommercialFlights", "AirplaneID", "dbo.Airplanes");
            DropForeignKey("dbo.Airports", "AdministrativeLocationID", "dbo.AdministrativeLocations");
            DropIndex("dbo.PrivateFlights", new[] { "DestinationAirport_AirportID" });
            DropIndex("dbo.PrivateFlights", new[] { "DepartureAirport_AirportID" });
            DropIndex("dbo.PrivateFlights", new[] { "AirplaneID" });
            DropIndex("dbo.CommercialFlights", new[] { "DestinationAirport_AirportID" });
            DropIndex("dbo.CommercialFlights", new[] { "DepartureAirport_AirportID" });
            DropIndex("dbo.CommercialFlights", new[] { "AirplaneID" });
            DropIndex("dbo.CommercialFlights", new[] { "CompanyID" });
            DropIndex("dbo.Airports", new[] { "AdministrativeLocationID" });
            DropTable("dbo.PrivateFlights");
            DropTable("dbo.CommercialFlights");
            DropTable("dbo.Airports");
        }
    }
}
