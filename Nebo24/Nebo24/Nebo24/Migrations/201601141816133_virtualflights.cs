namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class virtualflights : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommercialFlights", "Airport_AirportID", c => c.Int());
            AddColumn("dbo.CommercialFlights", "Airport_AirportID1", c => c.Int());
            CreateIndex("dbo.CommercialFlights", "Airport_AirportID");
            CreateIndex("dbo.CommercialFlights", "Airport_AirportID1");
            AddForeignKey("dbo.CommercialFlights", "Airport_AirportID", "dbo.Airports", "AirportID");
            AddForeignKey("dbo.CommercialFlights", "Airport_AirportID1", "dbo.Airports", "AirportID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CommercialFlights", "Airport_AirportID1", "dbo.Airports");
            DropForeignKey("dbo.CommercialFlights", "Airport_AirportID", "dbo.Airports");
            DropIndex("dbo.CommercialFlights", new[] { "Airport_AirportID1" });
            DropIndex("dbo.CommercialFlights", new[] { "Airport_AirportID" });
            DropColumn("dbo.CommercialFlights", "Airport_AirportID1");
            DropColumn("dbo.CommercialFlights", "Airport_AirportID");
        }
    }
}
