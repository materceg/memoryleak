namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAirplaneStuff : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Airplanes",
                c => new
                    {
                        AirplaneID = c.Int(nullable: false, identity: true),
                        Registration = c.String(nullable: false),
                        Type_AirplaneTypeID = c.Int(nullable: false),
                        Company_CompanyID = c.Int(),
                    })
                .PrimaryKey(t => t.AirplaneID)
                .ForeignKey("dbo.AirplaneTypes", t => t.Type_AirplaneTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Companies", t => t.Company_CompanyID)
                .Index(t => t.Type_AirplaneTypeID)
                .Index(t => t.Company_CompanyID);
            
            CreateTable(
                "dbo.AirplaneTypes",
                c => new
                    {
                        AirplaneTypeID = c.Int(nullable: false, identity: true),
                        ICAO = c.String(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AirplaneTypeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Airplanes", "Company_CompanyID", "dbo.Companies");
            DropForeignKey("dbo.Airplanes", "Type_AirplaneTypeID", "dbo.AirplaneTypes");
            DropIndex("dbo.Airplanes", new[] { "Company_CompanyID" });
            DropIndex("dbo.Airplanes", new[] { "Type_AirplaneTypeID" });
            DropTable("dbo.AirplaneTypes");
            DropTable("dbo.Airplanes");
        }
    }
}
