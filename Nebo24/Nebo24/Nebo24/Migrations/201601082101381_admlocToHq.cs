namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class admlocToHq : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CommercialFlights", "CompanyID", "dbo.Companies");
            DropIndex("dbo.CommercialFlights", new[] { "CompanyID" });
            RenameColumn(table: "dbo.Companies", name: "AdministrativeLocationID", newName: "Headquarters_AdministrativeLocationID");
            RenameIndex(table: "dbo.Companies", name: "IX_AdministrativeLocationID", newName: "IX_Headquarters_AdministrativeLocationID");
            AddColumn("dbo.Companies", "HeadquartersID", c => c.Int());
            DropColumn("dbo.CommercialFlights", "CompanyID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CommercialFlights", "CompanyID", c => c.Int());
            DropColumn("dbo.Companies", "HeadquartersID");
            RenameIndex(table: "dbo.Companies", name: "IX_Headquarters_AdministrativeLocationID", newName: "IX_AdministrativeLocationID");
            RenameColumn(table: "dbo.Companies", name: "Headquarters_AdministrativeLocationID", newName: "AdministrativeLocationID");
            CreateIndex("dbo.CommercialFlights", "CompanyID");
            AddForeignKey("dbo.CommercialFlights", "CompanyID", "dbo.Companies", "CompanyID");
        }
    }
}
