namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracija : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdministrativeLocation",
                c => new
                    {
                        AdministrativeLocationID = c.Int(nullable: false, identity: true),
                        City = c.String(nullable: false, maxLength: 96),
                        Country = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.AdministrativeLocationID);
            
            CreateTable(
                "dbo.Airplane",
                c => new
                    {
                        AirplaneID = c.Int(nullable: false, identity: true),
                        Registration = c.String(nullable: false),
                        AirplaneTypeID = c.Int(),
                        CompanyID = c.Int(),
                    })
                .PrimaryKey(t => t.AirplaneID)
                .ForeignKey("dbo.Company", t => t.CompanyID)
                .ForeignKey("dbo.AirplaneType", t => t.AirplaneTypeID)
                .Index(t => t.AirplaneTypeID)
                .Index(t => t.CompanyID);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        CompanyID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        HeadquartersAdmLocID = c.Int(),
                        HeadquartersGeoLocID = c.Int(),
                    })
                .PrimaryKey(t => t.CompanyID)
                .ForeignKey("dbo.AdministrativeLocation", t => t.HeadquartersAdmLocID)
                .ForeignKey("dbo.GeographicalLocation", t => t.HeadquartersGeoLocID)
                .Index(t => t.HeadquartersAdmLocID)
                .Index(t => t.HeadquartersGeoLocID);
            
            CreateTable(
                "dbo.GeographicalLocation",
                c => new
                    {
                        GeographicalLocationID = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.GeographicalLocationID);
            
            CreateTable(
                "dbo.AirplaneType",
                c => new
                    {
                        AirplaneTypeID = c.Int(nullable: false, identity: true),
                        ICAO = c.String(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AirplaneTypeID);
            
            CreateTable(
                "dbo.Airport",
                c => new
                    {
                        AirportID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        IATA = c.String(nullable: false),
                        AdministrativeLocationID = c.Int(),
                        GeoLocID = c.Int(),
                    })
                .PrimaryKey(t => t.AirportID)
                .ForeignKey("dbo.AdministrativeLocation", t => t.AdministrativeLocationID)
                .ForeignKey("dbo.GeographicalLocation", t => t.GeoLocID)
                .Index(t => t.AdministrativeLocationID)
                .Index(t => t.GeoLocID);
            
            CreateTable(
                "dbo.CommercialFlight",
                c => new
                    {
                        FlightID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        STD = c.DateTime(nullable: false),
                        ATD = c.DateTime(),
                        STA = c.DateTime(nullable: false),
                        ATA = c.DateTime(),
                        Callsign = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        AirplaneID = c.Int(),
                        DepartureAirportID = c.Int(),
                        DestinationAirportID = c.Int(),
                        Airport_AirportID = c.Int(),
                        Airport_AirportID1 = c.Int(),
                    })
                .PrimaryKey(t => t.FlightID)
                .ForeignKey("dbo.Airplane", t => t.AirplaneID)
                .ForeignKey("dbo.Airport", t => t.DepartureAirportID)
                .ForeignKey("dbo.Airport", t => t.DestinationAirportID)
                .ForeignKey("dbo.Airport", t => t.Airport_AirportID)
                .ForeignKey("dbo.Airport", t => t.Airport_AirportID1)
                .Index(t => t.AirplaneID)
                .Index(t => t.DepartureAirportID)
                .Index(t => t.DestinationAirportID)
                .Index(t => t.Airport_AirportID)
                .Index(t => t.Airport_AirportID1);
            
            CreateTable(
                "dbo.PrivateFlight",
                c => new
                    {
                        FlightID = c.Int(nullable: false, identity: true),
                        Callsign = c.String(nullable: false),
                        Status = c.Int(nullable: false),
                        AirplaneID = c.Int(),
                        DepartureAirportID = c.Int(),
                        DestinationAirportID = c.Int(),
                    })
                .PrimaryKey(t => t.FlightID)
                .ForeignKey("dbo.Airplane", t => t.AirplaneID)
                .ForeignKey("dbo.Airport", t => t.DepartureAirportID)
                .ForeignKey("dbo.Airport", t => t.DestinationAirportID)
                .Index(t => t.AirplaneID)
                .Index(t => t.DepartureAirportID)
                .Index(t => t.DestinationAirportID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrivateFlight", "DestinationAirportID", "dbo.Airport");
            DropForeignKey("dbo.PrivateFlight", "DepartureAirportID", "dbo.Airport");
            DropForeignKey("dbo.PrivateFlight", "AirplaneID", "dbo.Airplane");
            DropForeignKey("dbo.Airport", "GeoLocID", "dbo.GeographicalLocation");
            DropForeignKey("dbo.CommercialFlight", "Airport_AirportID1", "dbo.Airport");
            DropForeignKey("dbo.CommercialFlight", "Airport_AirportID", "dbo.Airport");
            DropForeignKey("dbo.CommercialFlight", "DestinationAirportID", "dbo.Airport");
            DropForeignKey("dbo.CommercialFlight", "DepartureAirportID", "dbo.Airport");
            DropForeignKey("dbo.CommercialFlight", "AirplaneID", "dbo.Airplane");
            DropForeignKey("dbo.Airport", "AdministrativeLocationID", "dbo.AdministrativeLocation");
            DropForeignKey("dbo.Airplane", "AirplaneTypeID", "dbo.AirplaneType");
            DropForeignKey("dbo.Company", "HeadquartersGeoLocID", "dbo.GeographicalLocation");
            DropForeignKey("dbo.Company", "HeadquartersAdmLocID", "dbo.AdministrativeLocation");
            DropForeignKey("dbo.Airplane", "CompanyID", "dbo.Company");
            DropIndex("dbo.PrivateFlight", new[] { "DestinationAirportID" });
            DropIndex("dbo.PrivateFlight", new[] { "DepartureAirportID" });
            DropIndex("dbo.PrivateFlight", new[] { "AirplaneID" });
            DropIndex("dbo.CommercialFlight", new[] { "Airport_AirportID1" });
            DropIndex("dbo.CommercialFlight", new[] { "Airport_AirportID" });
            DropIndex("dbo.CommercialFlight", new[] { "DestinationAirportID" });
            DropIndex("dbo.CommercialFlight", new[] { "DepartureAirportID" });
            DropIndex("dbo.CommercialFlight", new[] { "AirplaneID" });
            DropIndex("dbo.Airport", new[] { "GeoLocID" });
            DropIndex("dbo.Airport", new[] { "AdministrativeLocationID" });
            DropIndex("dbo.Company", new[] { "HeadquartersGeoLocID" });
            DropIndex("dbo.Company", new[] { "HeadquartersAdmLocID" });
            DropIndex("dbo.Airplane", new[] { "CompanyID" });
            DropIndex("dbo.Airplane", new[] { "AirplaneTypeID" });
            DropTable("dbo.PrivateFlight");
            DropTable("dbo.CommercialFlight");
            DropTable("dbo.Airport");
            DropTable("dbo.AirplaneType");
            DropTable("dbo.GeographicalLocation");
            DropTable("dbo.Company");
            DropTable("dbo.Airplane");
            DropTable("dbo.AdministrativeLocation");
        }
    }
}
