namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FlightFKFix : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CommercialFlights", "DepartureAirportID");
            DropColumn("dbo.CommercialFlights", "DestinationAirportID");
            DropColumn("dbo.PrivateFlights", "DepartureAirportID");
            DropColumn("dbo.PrivateFlights", "DestinationAirportID");
            RenameColumn(table: "dbo.CommercialFlights", name: "DepartureAirport_AirportID", newName: "DepartureAirportID");
            RenameColumn(table: "dbo.CommercialFlights", name: "DestinationAirport_AirportID", newName: "DestinationAirportID");
            RenameColumn(table: "dbo.PrivateFlights", name: "DepartureAirport_AirportID", newName: "DepartureAirportID");
            RenameColumn(table: "dbo.PrivateFlights", name: "DestinationAirport_AirportID", newName: "DestinationAirportID");
            RenameIndex(table: "dbo.CommercialFlights", name: "IX_DepartureAirport_AirportID", newName: "IX_DepartureAirportID");
            RenameIndex(table: "dbo.CommercialFlights", name: "IX_DestinationAirport_AirportID", newName: "IX_DestinationAirportID");
            RenameIndex(table: "dbo.PrivateFlights", name: "IX_DepartureAirport_AirportID", newName: "IX_DepartureAirportID");
            RenameIndex(table: "dbo.PrivateFlights", name: "IX_DestinationAirport_AirportID", newName: "IX_DestinationAirportID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.PrivateFlights", name: "IX_DestinationAirportID", newName: "IX_DestinationAirport_AirportID");
            RenameIndex(table: "dbo.PrivateFlights", name: "IX_DepartureAirportID", newName: "IX_DepartureAirport_AirportID");
            RenameIndex(table: "dbo.CommercialFlights", name: "IX_DestinationAirportID", newName: "IX_DestinationAirport_AirportID");
            RenameIndex(table: "dbo.CommercialFlights", name: "IX_DepartureAirportID", newName: "IX_DepartureAirport_AirportID");
            RenameColumn(table: "dbo.PrivateFlights", name: "DestinationAirportID", newName: "DestinationAirport_AirportID");
            RenameColumn(table: "dbo.PrivateFlights", name: "DepartureAirportID", newName: "DepartureAirport_AirportID");
            RenameColumn(table: "dbo.CommercialFlights", name: "DestinationAirportID", newName: "DestinationAirport_AirportID");
            RenameColumn(table: "dbo.CommercialFlights", name: "DepartureAirportID", newName: "DepartureAirport_AirportID");
            AddColumn("dbo.PrivateFlights", "DestinationAirportID", c => c.Int());
            AddColumn("dbo.PrivateFlights", "DepartureAirportID", c => c.Int());
            AddColumn("dbo.CommercialFlights", "DestinationAirportID", c => c.Int());
            AddColumn("dbo.CommercialFlights", "DepartureAirportID", c => c.Int());
        }
    }
}
