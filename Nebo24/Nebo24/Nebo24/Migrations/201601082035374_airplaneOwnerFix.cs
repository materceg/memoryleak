namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class airplaneOwnerFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Airplanes", "CompanyID", "dbo.Companies");
            DropIndex("dbo.Airplanes", new[] { "CompanyID" });
            RenameColumn(table: "dbo.Airplanes", name: "CompanyID", newName: "Owner_CompanyID");
            AddColumn("dbo.Airplanes", "OwnerID", c => c.Int(nullable: false));
            AlterColumn("dbo.Airplanes", "Owner_CompanyID", c => c.Int());
            CreateIndex("dbo.Airplanes", "Owner_CompanyID");
            AddForeignKey("dbo.Airplanes", "Owner_CompanyID", "dbo.Companies", "CompanyID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Airplanes", "Owner_CompanyID", "dbo.Companies");
            DropIndex("dbo.Airplanes", new[] { "Owner_CompanyID" });
            AlterColumn("dbo.Airplanes", "Owner_CompanyID", c => c.Int(nullable: false));
            DropColumn("dbo.Airplanes", "OwnerID");
            RenameColumn(table: "dbo.Airplanes", name: "Owner_CompanyID", newName: "CompanyID");
            CreateIndex("dbo.Airplanes", "CompanyID");
            AddForeignKey("dbo.Airplanes", "CompanyID", "dbo.Companies", "CompanyID", cascadeDelete: true);
        }
    }
}
