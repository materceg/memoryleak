namespace Nebo24.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hqToAdmLoc : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Companies", name: "Headquarters_AdministrativeLocationID", newName: "AdministrativeLocationID");
            RenameIndex(table: "dbo.Companies", name: "IX_Headquarters_AdministrativeLocationID", newName: "IX_AdministrativeLocationID");
            DropColumn("dbo.Companies", "HeadquartersID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Companies", "HeadquartersID", c => c.Int());
            RenameIndex(table: "dbo.Companies", name: "IX_AdministrativeLocationID", newName: "IX_Headquarters_AdministrativeLocationID");
            RenameColumn(table: "dbo.Companies", name: "AdministrativeLocationID", newName: "Headquarters_AdministrativeLocationID");
        }
    }
}
