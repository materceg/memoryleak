﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Nebo24.Models
{
    public class AdministrativeLocation
    {
        [Key]
        public int AdministrativeLocationID { get; set; }

        [Required]
        [Display(Name = "City", ResourceType = typeof(Resources.Domain))]
        [StringLength(96, MinimumLength = 2)]
        public string City { get; set; }

        [Required]
        [Display(Name = "Country", ResourceType = typeof(Resources.Domain))]
        [StringLength(64, MinimumLength = 3)]
        public string Country { get; set; }

        public string Location
        {
            get
            {
                return City + ", " + Country;
            }
        }
    }
}