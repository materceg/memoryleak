﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Nebo24.Models
{
    public class Company
    {
        [Key]
        public int CompanyID { get; set; }

        [Required]
        [Display(Name = "CompanyName", ShortName = "Name", ResourceType = typeof(Resources.Domain))]
        public string Name { get; set; }

        [Display(Name = "Headquarters", ResourceType = typeof(Resources.Domain))]
        public int? HeadquartersAdmLocID { get; set; }

        [ForeignKey("HeadquartersAdmLocID")]
        [JsonIgnore]
        public virtual AdministrativeLocation HeadquartersAdmLoc { get; set; }
        
        public int? HeadquartersGeoLocID { get; set; }
        
        [ForeignKey("HeadquartersGeoLocID")]
        [JsonIgnore]
        public virtual GeographicalLocation HeadquartersGeoLoc { get; set; }

        [JsonIgnore]
        public virtual ICollection<Airplane> Fleet { get; set; }
    }
}