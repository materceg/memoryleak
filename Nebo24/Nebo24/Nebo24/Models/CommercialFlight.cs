﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Nebo24.Models
{
    public class CommercialFlight : Flight
    {

        [Required]
        [Display(Name = "FlightNumber", ShortName = "Number", ResourceType = typeof(Resources.Domain))]
        public string Number { get; set; }
        /*
        public int? CompanyID { get; set; }

        public virtual Company Company { get; set; }
        */
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [Display(Name = "STDLong", ShortName = "STD", ResourceType = typeof(Resources.Domain))]
        
        public DateTime STD { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [Display(Name = "ATDLong", ShortName = "ATD", ResourceType = typeof(Resources.Domain))]
        public DateTime? ATD { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [Display(Name = "STALong", ShortName = "STA", ResourceType = typeof(Resources.Domain))]
        public DateTime STA { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [Display(Name = "ATALong", ShortName = "ATA", ResourceType = typeof(Resources.Domain))]
        public DateTime? ATA { get; set; }

    }
}