﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Nebo24.Models
{
    public class GeographicalLocation
    {
        [Key]
        public int GeographicalLocationID { get; set; }

        [Required]
        [Display(Name = "Latitude", ResourceType = typeof(Resources.Domain))]
        public double Latitude { get; set; }

        [Required]
        [Display(Name = "Longitude", ResourceType = typeof(Resources.Domain))]
        public decimal Longitude { get; set; }
    }
}