﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nebo24.Models
{
    public class Airplane
    {

        [Key]
        public int AirplaneID { get; set; }

        [Required]
        [Display(Name = "AirplaneRegistration", ResourceType = typeof(Resources.Domain))]
        public string Registration { get; set; }

        [Display(Name = "AirplaneType", ResourceType = typeof(Resources.Domain))]
        public int? AirplaneTypeID { get; set; }

        [ForeignKey("AirplaneTypeID")]
        public virtual AirplaneType Type { get; set; }

        /// <summary>Company foreign key</summary>
        /// 
        [Display(Name = "CompanyOwner", ResourceType = typeof(Resources.Domain))]
        public int? CompanyID { get; set; }

        /// <summary>
        /// Owner company
        /// </summary>
        [ForeignKey("CompanyID")]
        public virtual Company Company { get; set; }
    }
}