﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nebo24.Models
{
    public class AirplaneType
    {
        [Key]
        public int AirplaneTypeID { get; set; }

        [Required]
        [Display(Name = "ICAOLong", ShortName = "ICAO", ResourceType = typeof(Resources.Domain))]
        public string ICAO { get; set; }

        [Required]
        [Display(Name = "Name", ResourceType = typeof(Resources.Domain))]
        public string Name { get; set; }
        
        [Display(Name = "AverageSpeed", Description = "UsedToEstimateFlightDuration", ResourceType = typeof(Resources.Domain))]
        [Range(200, 1500, ErrorMessageResourceName = "IrrationalValue", ErrorMessageResourceType = typeof(Resources.Label) )]
        public double AverageSpeed { get; set; }

        [Display(Name = "MaximumSpeed", Description = "UsedToEstimateFlightDuration", ResourceType = typeof(Resources.Domain))]
        [Range(300, 2000, ErrorMessageResourceName = "IrrationalValue", ErrorMessageResourceType = typeof(Resources.Label))]
        public double MaximumSpeed { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(Name = "ImageURL", ResourceType = typeof(Resources.Label))]
        public string ImageURL { get; set; }
        
    }
}