﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nebo24.Models
{
    public abstract class Flight
    {

        [Key]
        public int FlightID { get; set; }

        [Required]
        [Display(Name = "Callsign", ResourceType = typeof(Resources.Domain))]
        public String Callsign { get; set; }

        [Required]
        [Display(Name = "FlightStatus", ResourceType = typeof(Resources.Domain))]
        public FlightStatus Status { get; set; }

        [Display(Name = "AirplaneRegistration", ResourceType = typeof(Resources.Domain))]
        public int? AirplaneID { get; set; }

        [ForeignKey("AirplaneID")]
        [Display(Name = "Airplane", ResourceType = typeof(Resources.Domain))]
        public virtual Airplane Airplane { get; set; }

        [Display(Name = "DepartureAirport", ResourceType = typeof(Resources.Domain))]
        public int? DepartureAirportID { get; set; }

        [ForeignKey("DepartureAirportID")]
        public virtual Airport DepartureAirport { get; set; }

        [Display(Name = "DestinationAirport", ResourceType = typeof(Resources.Domain))]
        public int? DestinationAirportID { get; set; }

        [ForeignKey("DestinationAirportID")]
        public virtual Airport DestinationAirport { get; set; }
        
    }
}