﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Nebo24.Models
{
    public class Airport
    {

        [Key]
        public int AirportID { get; set; }

        [Required]
        [Display(Name = "AirportName", ResourceType = typeof(Resources.Domain))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "IATA", ResourceType = typeof(Resources.Domain))]
        public string IATA { get; set; }

        [Display(Name = "CityAndCountry", ResourceType = typeof(Resources.Domain))]
        public int? AdministrativeLocationID { get; set; }
        
        [ForeignKey("AdministrativeLocationID")]
        public virtual AdministrativeLocation AdmLoc { get; set; }

        public int? GeoLocID { get; set; }
        
        [ForeignKey("GeoLocID")]
        public virtual GeographicalLocation GeoLoc { get; set; }

        [JsonIgnore]
        public virtual ICollection<CommercialFlight> DepartingFlights { get; set; }

        [JsonIgnore]
        public virtual ICollection<CommercialFlight> ArrivingFlights { get; set; }


    }
}