﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.DAL;
using System.Web.Mvc;

namespace Nebo24.Helpers
{
    public static class AdministrativeLocationHelper
    {
        public static void PopulateAdministrativelocationsDropDownList(DatabaseContext db, dynamic ViewBag, object selectedAdmLoc = null)
        {
            var AdmLocs = db.AdministrativeLocations.OrderBy(l => l.Country).ThenBy(l => l.City);
            ViewBag.AdministrativeLocationID = new SelectList(AdmLocs, "AdministrativeLocationID", "Location", selectedAdmLoc);
        }
    }
}