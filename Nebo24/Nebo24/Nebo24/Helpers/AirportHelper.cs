﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.DAL;
using System.Web.Mvc;

namespace Nebo24.Helpers
{
    public static class AirportHelper
    {
        //selectedAirport is an optional parameter, that is, if it isn't passed to the function, it will take on a default value (null in this case)
        public static void PopulateAirportsDropDownList(DatabaseContext db, dynamic ViewBag, object selectedDepartureAirport = null, object selectedDestinationAirport = null)
        {
            var Airports = db.Airports.OrderBy(a => a.Name);
            ViewBag.DepartureAirportID = new SelectList(Airports, "AirportID", "Name", selectedDepartureAirport);
            ViewBag.DestinationAirportID = new SelectList(Airports, "AirportID", "Name", selectedDestinationAirport);
        }

    }
}