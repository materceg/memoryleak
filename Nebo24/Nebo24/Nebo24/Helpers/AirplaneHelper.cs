﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.DAL;
using System.Web.Mvc;

namespace Nebo24.Helpers
{
    public class AirplaneHelper
    {
        public static void PopulateAirplaneDropDownList(DatabaseContext db, dynamic ViewBag, object selectedAirplane = null)
        {
            var Airplanes = db.Airplanes;
            ViewBag.AirplaneID = new SelectList(Airplanes, "AirplaneID", "Registration", selectedAirplane);
        }

    }
}