﻿using Nebo24.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nebo24.Helpers
{
    public class CommercialFlightCalculator
    {
        public static Dictionary<int, double?> getDelays(IQueryable<CommercialFlight> flightList)
        {
            Dictionary<int, double?> delays = new Dictionary<int, double?>();
            foreach (CommercialFlight flight in flightList)
            {
                if (flight.ATA.HasValue)
                {
                    if (flight.ATA - flight.STA >= TimeSpan.FromMinutes(30))
                        delays.Add(flight.FlightID, (flight.ATA - flight.STA).Value.TotalMinutes);
                }
                else if (flight.ATD.HasValue)
                {
                    if (flight.ATD - flight.STD >= TimeSpan.FromMinutes(15))
                        delays.Add(flight.FlightID, (flight.ATD - flight.STD).Value.TotalMinutes);
                }
                else
                    delays.Add(flight.FlightID, null);
            }
            return delays;
        }


        public static void getStatus(IQueryable<CommercialFlight> flightList)
        {
            foreach (CommercialFlight flight in flightList)
            {
                if (!flight.ATA.HasValue && !flight.ATD.HasValue)
                    flight.Status = FlightStatus.Pending;

                else if (flight.ATA.HasValue && flight.ATD.HasValue)
                {
                    if (flight.ATD > DateTime.Now) // if future takeoff
                        flight.Status = FlightStatus.Pending;
                    else if (flight.ATA > DateTime.Now) //if takeoff is in the past, and landing is in the future
                        flight.Status = FlightStatus.TookOff;
                    else //if both takeoff and landing are in the past
                        flight.Status = FlightStatus.Landed;
                }

                else if (flight.ATA.HasValue) // ATD isn't set, so the surely plane hasnt taken off yet              
                    flight.Status = FlightStatus.Pending;

                else // then, ATD must have a value 
                {
                    if (flight.ATD > DateTime.Now) //if takeoff is in the future
                        flight.Status = FlightStatus.Pending;
                    else //takeoff is in the past
                        flight.Status = FlightStatus.TookOff;
                }

            }
        }

        public static void getStatus(CommercialFlight flight)
        {

            if (!flight.ATA.HasValue && !flight.ATD.HasValue)
                flight.Status = FlightStatus.Pending;

            else if (flight.ATA.HasValue && flight.ATD.HasValue)
            {
                if (flight.ATD > DateTime.Now) // if future takeoff
                    flight.Status = FlightStatus.Pending;
                else if (flight.ATA > DateTime.Now) //if takeoff is in the past, and landing is in the future
                    flight.Status = FlightStatus.TookOff;
                else //if both takeoff and landing are in the past
                    flight.Status = FlightStatus.Landed;
            }

            else if (flight.ATA.HasValue) // ATD isn't set, so the surely plane hasnt taken off yet              
                flight.Status = FlightStatus.Pending;

            else // then, ATD must have a value 
            {
                if (flight.ATD > DateTime.Now) //if takeoff is in the future
                    flight.Status = FlightStatus.Pending;
                else //takeoff is in the past
                    flight.Status = FlightStatus.TookOff;



                /* if (!flight.ATA.HasValue && !flight.ATD.HasValue)
                     flight.Status = FlightStatus.Pending;
                 else if (flight.ATA.HasValue)
                     flight.Status = FlightStatus.Landed;
                 else if (flight.ATD.HasValue)
                     flight.Status = FlightStatus.TookOff;    
                     */
            }

        }
    }
}