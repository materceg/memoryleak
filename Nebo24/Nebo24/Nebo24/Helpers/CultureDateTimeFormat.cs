﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace Nebo24.Helpers
{
    public static class CultureDateTimeFormat
    {

        public static string ShortDateTimePattern()
        {
            switch (Thread.CurrentThread.CurrentCulture.Name)
            {
                /*
                    return "DD.MM.YYYY hh:mm";
                case "de-DE":
                case "de-AT":
                case "it-IT":
                */
                case "en-US":
                    return ("MM/DD/YYYY hh:mm A");
                case "en-UK":
                    return ("DD/MM/YYYY hh:mm A");
                case "hr-HR":
                    return ("DD.MM.YYYY HH:mm");
                default:
                    return ("DD/MM/YYYY HH:mm");
            }
        }
    }
}