﻿function initMap(lat, lng) {
    if (lat == 0 && lng == 0)
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                updateCoordinates(lat, lng);
                var LatLng = { lat: lat, lng: lng };
                map.setCenter(LatLng);
                marker.setPosition(LatLng);
            }, function () {
                handleNoGeolocation(browserSupportFlag);
            });
        }
    }
    var LatLng = { lat: lat, lng: lng }
    var mapOptions = {
        zoom: 4,
        center: LatLng,
        disableDefaultUI: true
    }
    var map = new google.maps.Map(document.getElementById("small-map"),
            mapOptions);
    var marker = new google.maps.Marker({
        draggable: true,
        position: LatLng,
        map: map
    });

    var Latitude = document.getElementById("GeoLoc_Latitude");
    if (Latitude == null) {
        Latitude = document.getElementById("HeadquartersGeoLoc_Latitude");
    }
    var Longitude = document.getElementById("GeoLoc_Longitude");
    if (Longitude == null) {
        Longitude = document.getElementById("HeadquartersGeoLoc_Longitude");
    }

    // Sets the new marker position lat and lng to the input fields
    google.maps.event.addListener(marker, 'dragend', function (evt) {
        Latitude.value = evt.latLng.lat();
        Longitude.value = evt.latLng.lng();
    });

    var updateCoordinates = function (lat, lng) {
        Latitude.value = lat;
        Longitude.value = lng;

    }

    // Sets the new marker position and pans the map center to the new marker position
    // taken from the input fields
    var updateMarker = function () {
        var newPosition = new google.maps.LatLng(parseFloat(Latitude.value), parseFloat(Longitude.value))
        map.panTo(newPosition);
        marker.setPosition(newPosition)
    }
    Latitude.addEventListener('change', updateMarker);
    Longitude.addEventListener('change', updateMarker);
    
}