﻿var AirplaneTypeID = document.getElementById('AirplaneTypeID');

function loadAirplaneTypeInformation() {
    $.get('/api/AirplaneType/GetByAirplaneTypeId?id=' + AirplaneTypeID.value, function (data) {

        // Display Airplane Type information
        var content = '<div class="col-sm-12 col-md-6 col-lg-8 col-xl-10"><dl><dt>';
        if (data.Name) {
            content += '<h3>' + data.Name + '</h3>'
        }
        content += '</dt><dd>' + data.ICAO + '<dd></dl></div>';
        if (data.ImageURL) {
            content += '<img class="img-responsive img-thumbnail col-sm-12 col-md-6 col-lg-4 col-xl-2" src="' + data.ImageURL + '" />';
        }
        var element = document.getElementById('AirplaneTypeInformationContainer');
        element.innerHTML = content;
        document.getElementById('AirplaneTypeInformation').style.display = 'block';
    });
}

$(document).ready(function () {
    loadAirplaneTypeInformation();
    AirplaneTypeID.addEventListener('change', loadAirplaneTypeInformation);
    document.getElementById('AirplaneTypeCloseButton').addEventListener('click', function () {
        document.getElementById('AirplaneTypeInformation').style.display = 'none';
        document.getElementById('AirplaneTypeInformationContainer').innerHTML = '';
    });
});