﻿

var averageSpeed;
var maximumSpeed;
var distance;
var dateMinimum = '1/1/1900';
var AirplaneID = document.getElementById('AirplaneID');

function loadAirplaneTypeInformation() {
    $.get('/api/AirplaneType/GetByAirplaneId?id=' + AirplaneID.value, function (data) {

        // Display Airplane Type information
        var content = '<div class="col-sm-12 col-md-6 col-lg-8 col-xl-10"><dl><dt>';
        if (data.Name) {
            content += '<h3>' + data.Name + '</h3>'
        }
        content += '</dt><dd>' + data.ICAO + '<dd></dl></div>';
        if (data.ImageURL) {
            content += '<img class="img-responsive img-thumbnail col-sm-12 col-md-6 col-lg-4 col-xl-2" src="' + data.ImageURL + '" />';
        }
        var element = document.getElementById('AirplaneTypeInformationContainer');
        element.innerHTML = content;
        document.getElementById('AirplaneTypeInformation').style.display = 'block';

        // Use the Maximum and Average Speeds and Coordinates to estimate the duration of the flight
        averageSpeed = data.AverageSpeed;
        maximumSpeed = data.MaximumSpeed;
        document.getElementById('AverageSpeed').innerHTML = averageSpeed;
        document.getElementById('MaximumSpeed').innerHTML = maximumSpeed;
    });
}

var DepartureAirportID = document.getElementById('DepartureAirportID');
var DestinationAirportID = document.getElementById('DestinationAirportID');
var FlightDistanceCalculation = document.getElementById('FlightDistanceCalculation');

function updateContext() {
    
    $.get('/api/Airport/GetPairByAirportIds?idA=' + DepartureAirportID.value + '&idB=' + DestinationAirportID.value, function (data) {
        var distance = calculateDistance(data[0].GeoLoc.Latitude, data[0].GeoLoc.Longitude, data[1].GeoLoc.Latitude, data[1].GeoLoc.Longitude);

        FlightDistanceCalculation.innerHTML = distance;

        if (distance)
        {
            var minDuration = (distance / maximumSpeed) * 3600;
            var estDuration = (distance / averageSpeed) * 3600;
            var minArrival = new moment(new Date().valueOf() + minDuration * 1000);
            var estArrival = new moment(new Date().valueOf() + estDuration * 1000);
        }
        else {
            estArrival = ' - ';
            minArrival = ' - ';
        }
        estArrivalF = moment(estArrival).format(format);
        minArrivalF = moment(minArrival).format(format);
        document.getElementById('EstimatedArrivalTime').innerHTML = estArrivalF;
        document.getElementById('EarlistPossibleArrivalTime').innerHTML = minArrivalF;

        var STDInput = document.getElementById('STA');
        var STAInput = document.getElementById('STA');
    });
}

$(document).ready(function () {
    loadAirplaneTypeInformation();
    updateContext();
    DepartureAirportID.addEventListener('change', updateContext);
    DestinationAirportID.addEventListener('change', updateContext);

    AirplaneID.addEventListener('change', loadAirplaneTypeInformation);
    document.getElementById('AirplaneTypeCloseButton').addEventListener('click', function () {
        document.getElementById('AirplaneTypeInformation').style.display = 'none';
        document.getElementById('AirplaneTypeInformationContainer').innerHTML = '';
    });

});