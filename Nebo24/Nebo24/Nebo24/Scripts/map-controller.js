﻿
var mapCanvas = document.getElementById('map-canvas');
var map;
var positionAndTimeData = [];
function getNow () {
    if (/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)) {
        return moment().add(1, 'hours');
    }
    else {
        return moment();
    }
}

function initialize(details, departures, arrivals) {
    var lat, lng;
    lat = 45.50;
    lng = 15.58;

    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 4,
        minZoom: 3,
        maxZoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(mapCanvas, mapOptions);

    $.get('/api/Airport/GetAll', function (data) {
        var airportIconUrl = '/Content/Images/airport-marker.png'
        var infowindow = new google.maps.InfoWindow();
        $.each(data, function () {

            var LatLng = { lat: this.GeoLoc.Latitude, lng: this.GeoLoc.Longitude }
            var url = '/Airport/Details/' + this.AirportID;
            var contentString = '<div class="header"><h3>' + this.Name + '</h3><small>' + this.IATA + '</small></div>';
            contentString += '<address class="large">' + this.AdmLoc.Location + '</address>';
            contentString += '<div class="btn-group">';
            contentString += '<a href="' + url + '" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-info-sign"></span> ' + details + '</a>';
            contentString += '<a href="' + url + '#departures" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-export"></span> ' + departures + '</a>';
            contentString += '<a href="' + url + '#arrivals" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-import"></span> ' + arrivals + '</a>';
            contentString += '</div>';

            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                icon: {
                    url: airportIconUrl,
                },
                scale: .1,
                title: this.Name + ' (' + this.IATA + '), ' + this.AdmLoc.Location,
                url: url
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
        });
    });

    function calculateFraction(arrival, departure) {
        return (parseFloat(getNow() - departure) / parseFloat(arrival - departure));
    }

    function calculatePosition(departure, arrival, ALat, ALng, BLat, BLng) {
        var fraction = calculateFraction(arrival, departure);
        if (fraction > 1) {
            fraction = 1;
        }
        var lat = ALat + (BLat - ALat) * fraction;
        var lng = ALng + (BLng - ALng) * fraction;
        return {lat: lat, lng: lng};
    }

    $.get('api/FlightTracking/GetFlightsInProgress', function (data) {
        var airplaneIconUrl = '/Content/Images/airplane-marker.svg'
        var infowindow = new google.maps.InfoWindow();
        $.each(data, function () {
            var ALat = this.DepartureAirport.GeoLoc.Latitude;
            var ALng = this.DepartureAirport.GeoLoc.Longitude;
            var BLat = this.DestinationAirport.GeoLoc.Latitude;
            var BLng = this.DestinationAirport.GeoLoc.Longitude;
            
            var departure = new Date( (this.ATD) ? this.ATD : this.STD );
            var arrival = new Date((this.ATA) ? this.ATA : this.STA);

            var LatLng = calculatePosition(departure, arrival, ALat, ALng, BLat, BLng);

            if (true/*arrival >= getNow()*/) {
                var url = '/CommercialFlight/Details/' + this.FlightID;
                var contentString = '<div style="min-width: 550px !important;">';
                contentString += '<div class="header"><img class="img-responsive img-thumbnail pull-right" src="' + this.Airplane.Type.ImageURL + '" style="max-width:250px; margin-bottom: 20px;" />';
                contentString += '<h3>' + this.Number + ' (' + this.Callsign + ')</h3></div>';
                contentString += '<h4><a href="/Company/Details/' + this.Airplane.Company.CompanyID + '">' + this.Airplane.Company.Name + '</a></h4>';
                contentString += '<address class="large"><div><span class="glyphicon glyphicon-globe"></span> ' + this.DepartureAirport.AdmLoc.Location;
                contentString += ' <span class="glyphicon glyphicon-arrow-right"></span> ';
                contentString += this.DestinationAirport.AdmLoc.Location + '</div>';
                contentString += '<div><span class="glyphicon glyphicon-time"></span> ' + moment(this.ATD).format(format) + ' <span class="glyphicon glyphicon-arrow-right"></span> ' + moment(this.STA).format(format) + '</div></address>';
                contentString += '<div class="divided"></div><div class="btn-group col-xs-12">';
                contentString += '<a href="' + url + '" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-info-sign"></span> ' + details + '</a>';
                contentString += '<a href="/Airport/Details/' + this.DepartureAirport.AirportID + '" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-export"></span> ' + this.DepartureAirport.Name + '</a>';
                contentString += '<a href="/Airport/Details/' + this.DestinationAirport.AirportID + '" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-import"></span> ' + this.DestinationAirport.Name + '</a>';
                contentString += '</div>';

                var rotation = ( Math.atan2(BLng - ALng, BLat - ALat) - Math.PI / 2 ) * 180 / Math.PI;

                var icon = {

                    path: "M510,255c0-20.4-17.85-38.25-38.25-38.25H331.5L204,12.75h-51l63.75,204H76.5l-38.25-51H0L25.5,255L0,344.25h38.25l38.25-51h140.25l-63.75,204h51l127.5-204h140.25C492.15,293.25,510,275.4,510,255z",
                    fillColor: 'blue',
                    fillOpacity: .5,
                    strokeWeight: 1,
                    scale: .075,
                    rotation: rotation
                }

                var marker = new google.maps.Marker({
                    position: LatLng,
                    map: map,
                    icon: icon,
                    title: this.Callsign + ' (' + this.Status + ')',
                    id: this.FlightID,
                    url: url
                });

                positionAndTimeData.push( { marker: marker, ALat: ALat, ALng: ALng, BLat: BLat, BLng: BLng, departure: departure, arrival: arrival } );

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                });

            }
        });
    });

    function getIntervalTime() {
        switch (map.getZoom())
        {
            case 4:
            case 5:
            case 6:
                return 3840;
            case 7:
                return 1920;
            case 8:
                return 960;
            case 10:
                return 480;
            case 11:
            case 12:
                return 240;
            case 13:
            case 14:
                return 120;
            case 15:
            case 16:
                return 60;
            default:
                return 960;
        }
    }

    function updateFlights () {
        $.each(positionAndTimeData, function () {
            var marker = this.marker;
            var LatLng = calculatePosition(this.departure, this.arrival, this.ALat, this.ALng, this.BLat, this.BLng);
            marker.setPosition(LatLng);
        });
    };

    var interval = getIntervalTime();

    map.addListener('zoom_changed', function () {
        interval = getIntervalTime();
        //document.getElementById('toggleMapZoom').innerHTML = interval; // debug
    });

    var removePastFlight = setInterval(function () {
        $.each(positionAndTimeData, function (index, value) {
            if (value.ATA > new Date() || value.STA > new Date()) {
                positionAndTimeData.splice(index, 1);
                alert(index);
            }
        });
    }, 2000)

    function callback() {
        updateFlights();
        setTimeout( callback, interval );
    }

    setTimeout( callback, interval );

}

var mapContainer = document.getElementById('map-container');
var mapResizer = document.getElementById('map-resizer');
var mapCenter, startY, startHeight;


// Map resize functionality

function initResize(e) {
    startY = e.clientY;
    mapCenter = map.getCenter();
    startHeight = parseInt(document.defaultView.getComputedStyle(mapContainer).height, 10);
    document.documentElement.addEventListener('mousemove', doResize, false);
    document.documentElement.addEventListener('mouseup', stopResize, false);
}

function doResize(e) {
    var newHeight = (startHeight + e.clientY - startY) + 'px';
    mapContainer.style.height = newHeight;
    map.setCenter(mapCenter);
}

function stopResize(e) {
    document.documentElement.removeEventListener('mousemove', doResize, false);
    document.documentElement.removeEventListener('mouseup', stopResize, false);
    google.maps.event.trigger(map, 'resize');
    map.setCenter(mapCenter);
}


var toggleMap = document.getElementById('toggleMap');

toggleMap.addEventListener('click', function(event) {
    event.preventDefault();
    if (mapContainer.style.display == '')
        mapContainer.style.display = 'none';
    else
        mapContainer.style.display = '';
});

mapResizer.addEventListener('mousedown', initResize, false);
mapContainer.style.minHeight = screen.availHeight / 2 - 100 + 'px';
mapContainer.style.maxHeight = screen.availHeight - 200 + 'px';