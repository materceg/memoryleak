﻿function initializeTimeSpan(restrict, format) {
    var DTFrom = $('#FilterDateTimeFrom-input');
    var DTTo = $('#FilterDateTimeTo-input');

    var dateMinimum;
    var dateMaximum;

    if (restrict) {
        dateMinimum = new Date();
        dateMinimum.setDate(dateMinimum.getDate() - 7);
        dateMinimum.setHours(0, 0, 0, 0);

        dateMaximum = new Date();
        dateMaximum.setDate(dateMaximum.getDate() + 7);
        dateMaximum.setHours(0, 0, 0, 0);
    }
    else {
        dateMinimum = new Date("1/1/1900");
        dateMaximum = new Date("1/1/2099");
    }

    DTFrom.datetimepicker({
        minDate: dateMinimum,
        maxDate: dateMaximum,
        format: format
    });
    DTTo.datetimepicker({
        minDate: dateMinimum,
        maxDate: dateMaximum,
        format: format,
        useCurrent: false //Important! See issue #1075
    });

    DTFrom.on("dp.change", function (e) {
        DTTo.data("DateTimePicker").minDate(e.date);
    });
    DTTo.on("dp.change", function (e) {
        DTFrom.data("DateTimePicker").maxDate(e.date);
    });
};