﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nebo24.Startup))]
namespace Nebo24
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
