﻿using System.Collections.Generic;
using Nebo24.Models;

namespace Nebo24.ViewModels
{
    public class AirportViewModel
    {
        public Airport Airport { get; set; }

        public double AverageDepartureDelay { get; set; }

        public double AverageArrivalDelay { get; set; }

        public CommercialFlightsViewModel Departures { get; set; }

        public CommercialFlightsViewModel Arrivals { get; set; }

        public FlightsFilterViewModel Filter { get; set; }
    }
}