﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.Models;

namespace Nebo24.ViewModels
{
    public class CommercialFlightViewModel
    {
        public CommercialFlight CommercialFlight { get; set; }

        public double? Delay { get; set; }

        public Airport DepartureAirport { get; set; }

        public Airport DestinationAirport { get; set; }       

    }
}