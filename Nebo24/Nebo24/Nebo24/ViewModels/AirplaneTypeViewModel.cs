﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.Models;
using Nebo24.ViewModels;

namespace Nebo24.ViewModels
{
    public class AirplaneTypeViewModel
    {
        public AirplaneType AirplaneType { get; set; }
        public virtual IEnumerable<Airplane> Airplanes { get; set; }
        public virtual CommercialFlightsViewModel Flights { get; set; }
    }
}