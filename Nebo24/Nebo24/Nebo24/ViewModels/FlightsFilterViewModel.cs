﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nebo24.ViewModels
{
    public class FlightsFilterViewModel
    {
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [Display(Name = "DepartureTime", ResourceType = typeof(Resources.Domain))]
        public DateTime? DepartureTime { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        [Display(Name = "ArrivalTime", ResourceType = typeof(Resources.Domain))]
        public DateTime? ArrivalTime { get; set; }
        public int? DepartureAirportID { get; set; }
        public int? DestinationAirportID { get; set; }
    }
}