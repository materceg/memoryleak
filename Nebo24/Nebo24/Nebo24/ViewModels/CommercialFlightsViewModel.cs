﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.Models;
using Nebo24.ViewModels;

namespace Nebo24.ViewModels
{
    public class CommercialFlightsViewModel
    {
        public IQueryable<CommercialFlight> CommercialFlights { get; set; }

        public Dictionary<int, double?> Delay { get; set; }

        public FlightsFilterViewModel Filter { get; set; }
    }
}