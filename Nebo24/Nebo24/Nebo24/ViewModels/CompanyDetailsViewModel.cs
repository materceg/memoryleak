﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nebo24.DAL;
using System.Web.Mvc;
using Nebo24.Models;

namespace Nebo24.ViewModels
{
    public class CompanyDetailsViewModel
    {
        public IEnumerable<Airplane> Fleet { get; set; }

        public Company Company { get; set; }

    }
}