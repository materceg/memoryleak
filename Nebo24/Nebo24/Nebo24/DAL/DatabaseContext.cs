﻿using Nebo24.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Nebo24.DAL
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("DatabaseContext")
        {
        }

        public DbSet<GeographicalLocation> GeographicalLocations { get; set; }
        public DbSet<AdministrativeLocation> AdministrativeLocations { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<AirplaneType> AirplaneTypes { get; set; }
        public DbSet<Airplane> Airplanes { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<CommercialFlight> CommercialFlights { get; set; }
        public DbSet<PrivateFlight> PrivateFlights { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        
    }
}